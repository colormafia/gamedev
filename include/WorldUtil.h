#ifndef __WorldUtil_h_
#define __WorldUtil_h_

class Spacecraft;

/// Utility class for getting information of the world.
/// - raycast
/// - position of other spacecrafts.
class WorldUtil 
{
public:
	/// Used to identify different collision objects to enable filtering of collision.
	enum CollisionFilter
	{
		FILTER_DYNAMIC_SPACECRAFT = (1 << 0),
		FILTER_DYNAMIC_ROCKETS = (1 << 1),
		FILTER_STATIC_OBSTACLES = (1 << 2),
		FILTER_STATIC_GROUND = (1 << 3),

		FILTER_DYNAMIC = FILTER_DYNAMIC_SPACECRAFT | FILTER_DYNAMIC_ROCKETS,
		FILTER_STATIC = FILTER_STATIC_OBSTACLES | FILTER_STATIC_GROUND,
		FILTER_ALL = FILTER_DYNAMIC | FILTER_STATIC
	};

	/// performs a shape cast with the shape of the given spacecraft.
	static bool rayCast(Spacecraft* spacecraft, float lookAheadTime, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter = FILTER_STATIC_OBSTACLES);
	
	/// performs a shape cast with the given shape.
	static bool rayCast(const btConvexShape* shape, const btTransform& from, const btTransform& to, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter = FILTER_STATIC_OBSTACLES);
	
	/// performs a ray cast.
	static bool rayCast(const Ogre::Vector3 from, const Ogre::Vector3 to, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter = FILTER_STATIC_OBSTACLES);

	/// returns a const reference to all Spacecrafts in the game.
	static const std::vector<Spacecraft*>& getAllSpacecrafts();
};


#endif