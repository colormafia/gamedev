#ifndef __MathUtil_h_
#define __MAthUtil_h_

class MathUtil
{
public:
	/// maps an angle between -PI (-180�) und PI (180�) 
	static float mapAngle(float angle)
	{
		angle = fmod(angle + Ogre::Math::PI, Ogre::Math::PI * 2);
		if (angle < 0)
			angle += Ogre::Math::PI * 2;
		return angle - Ogre::Math::PI;
	}

	/// maps an value between 0 and maximum
	static float mapRange(float value, float maximum)
	{
		value = fmod(value, maximum);

		if (value < 0)
			value += maximum;
		return value;
	}

	/// maps an value between 0 and maximum
	static int mapRange(int value, int maximum)
	{
		value = value % maximum;

		if (value < 0)
			value += maximum;
		return value;
	}

	static bool quadraticFormula(float a, float b, float c, float& t1, float& t2)
	{
		float q = b * b - 4 * a * c;

		if (q >= 0)
		{
			float sq = (float) sqrt(q);
			float d = 1 / (2 * a);
			t1 = (-b + sq) * d;
			t2 = (-b - sq) * d;
			return true;
		}
		else
		{
			return false;
		}
	}

	/// Performs a sweep test with 2 given spheres
	/// param t0 time when collision starts
	/// param t1 time when collision ends
	static bool sphereSweepTest(float radiusA, const Ogre::Vector3& posA, const Ogre::Vector3& velA,
								float radiusB, const Ogre::Vector3& posB, const Ogre::Vector3& velB,
								float& t0, float& t1)
	{
		Ogre::Vector3 AB = posB - posA;
		Ogre::Vector3 vab = velB - velA;

		float rab = radiusA + radiusB;
		float a = vab.dotProduct(vab);
		float b = 2 * vab.dotProduct(AB);
		float c = AB.dotProduct(AB) - rab * rab;

		if (AB.dotProduct(AB) <= rab * rab)
		{
			t0 = 0;
			t1 = 0;
			return true;
		}

		if (a == 0.0f)
		{
			return false;
		}

		if (quadraticFormula(a, b, c, t0, t1))
		{
			if (t0 > t1)
			{
				std::swap(t0, t1);
			}
			return true;
		}

		return false;
	}

	/// Returns the closes point on the line based on the projected position.
	static float getClosestPointOnLineSegment(const Ogre::Vector3& a, const Ogre::Vector3& b, const Ogre::Vector3& p)
	{
		Ogre::Vector3 AP = p - a;       //Vector from A to P   
		Ogre::Vector3 AB = b - a;       //Vector from A to B  

		float magnitudeAB = AB.squaredLength();     //Magnitude of AB vector (it's length squared)     
		float ABAPproduct = AP.dotProduct(AB);    //The DOT product of a_to_p and a_to_b     
		float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

		return Ogre::Math::Clamp(distance, 0.0f, 1.0f);
	}
};

#endif