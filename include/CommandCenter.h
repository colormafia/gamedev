 #ifndef __CommandCenter_h_
 #define __CommandCenter_h_ 

#include "ICollider.h"

/// Wall obstacle in the world
class CommandCenter: public ICollider
{
public:
	/// creates a wall from the startPos to the endPos
	CommandCenter(Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const Ogre::Vector3& position);

	virtual ~CommandCenter() {};

	virtual void onCollision(ICollider* collider) {};
private:
	static int msInstanceCounter;

	OgreBulletDynamics::RigidBody* mBody;
	OgreBulletCollisions::BoxCollisionShape *mShape;
	Ogre::SceneNode* mNode;
};


#endif