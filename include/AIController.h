 #ifndef __AIController_h_
 #define __AIController_h_ 

#include "SpacecraftController.h"
#include "Path.h"


class AIController: public SpacecraftController
{
public:
	AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, const Path& patrol);

	virtual void update(float delta);

private:
	Spacecraft* mHumanSpacecraft;

	Path mPath;
	float mCurrentParam;
};


#endif