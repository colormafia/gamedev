#version 120

///
/// Blur gaussiano verticale
/// Da utilizzare in catena con quello orizzontale per un blur completo
///

uniform sampler2D tex;
uniform float blurSize;

vec2 pos[11] = vec2[](
    vec2(0.0, -5.0),
    vec2(0.0, -4.0),
    vec2(0.0, -3.0),
    vec2(0.0, -2.0),
    vec2(0.0, -1.0),
    vec2(0.0,  0.0),
    vec2(0.0,  1.0),
    vec2(0.0,  2.0),
    vec2(0.0,  3.0),
    vec2(0.0,  4.0),
    vec2(0.0,  5.0)
);

float samples[11] = float[](
    0.01222447,
    0.02783468,
    0.06559061,
    0.12097757,
    0.17466632,
    0.19741265,
    0.17466632,
    0.12097757,
    0.06559061,
    0.02783468,
    0.01222447
);

void main() {
    vec2 coord = vec2(gl_TexCoord[0]);
    vec4 sum = vec4( 0 );
    float bs = blurSize / 1024.0;
    int i = 0;
    for(; i != 11; ++i) {
        sum += texture2D(tex, coord + (pos[i] * bs)) * samples[i];
    }
    gl_FragColor = sum;
}
