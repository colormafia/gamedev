#include "stdafx.h"

#include <stack>
#include "NavigationGraph.h"
#include "NavigationNode.h"
#include "Connection.h"
#include "PathfindingList.h"
#include "NavigationGraphDebugDisplay.h"
#include "DebugDisplay.h"

using namespace Ogre;
using namespace OgreBulletDynamics;
using namespace OgreBulletCollisions;

template<> NavigationGraph* Ogre::Singleton<NavigationGraph>::msSingleton = 0;


NavigationGraph::NavigationGraph(SceneManager* sceneMgr, int _x, int _z, int _width, int _height):
	origin((float) _x, 0.0f, (float) _z),
	width(_width),
	height(_height)
{
	mDebugDisplay = new NavigationGraphDebugDisplay(sceneMgr, 0.5f);
	gridWidth = (int) floor(width / NavigationNode::NODE_SIZE);
	gridDepth = (int) floor(height / NavigationNode::NODE_SIZE);
	
	int gridSize = gridWidth * gridDepth;
	grid.reserve(gridSize);
	
	for (int i = 0; i < gridSize; i++)
	{
		grid.push_back(NULL);
	}
}


NavigationGraph::~NavigationGraph()
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		delete grid[i];
	}

	grid.clear();
}

NavigationGraph* NavigationGraph::getSingletonPtr(void)
{
    return msSingleton;
}
NavigationGraph& NavigationGraph::getSingleton(void)
{  
    assert( msSingleton );  return ( *msSingleton );  
}

void NavigationGraph::setDebugDisplayEnabled(bool enable)
{
	mDebugDisplay->setEnabled(enable);
}

bool NavigationGraph::checkSpaceForNode(OgreBulletDynamics::DynamicsWorld* world, const Vector3& position) const
{
	const Vector3 offset[] = { 
		Vector3(-NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(0.0f, 0.0f, -NavigationNode::NODE_SIZE_HALF*0.5f), 
		Vector3(0.0f, 0.0f, NavigationNode::NODE_SIZE_HALF*0.5f) 
	};

	int blocked = 0;

	// send multiple rays to check if spot is free.
	for (int i = 0; i < 4; i++)
	{
		btVector3 start(position.x + offset[i].x, 100.0f, position.z + offset[i].z);
		btVector3 end(position.x + offset[i].x, 0.0f, position.z + offset[i].z);
			
		btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);
 
		// Perform raycast
		world->getBulletCollisionWorld()->rayTest(start, end, rayCallback);
 
		if(!rayCallback.hasHit()) 
		{
			return true;
		}
	}

	return false;
}

bool NavigationGraph::rayTest(OgreBulletDynamics::DynamicsWorld* world, const Vector3& start, const Vector3& end) const
{
	btVector3 startBt = OgreBtConverter::to(start);
	btVector3 endBt = OgreBtConverter::to(end);

	btCollisionWorld::ClosestRayResultCallback rayCallback(startBt, endBt);
 
	// Perform raycast
	world->getBulletCollisionWorld()->rayTest(startBt, endBt, rayCallback);
 
	return rayCallback.hasHit();
}


void NavigationGraph::calcGraph(OgreBulletDynamics::DynamicsWorld* world)
{
	for (int z = 0; z < gridDepth; z++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			Vector3 pos((float) (x * NavigationNode::NODE_SIZE), 5.0f,
					 (float) (z * NavigationNode::NODE_SIZE));

			pos += origin;

			if(!checkSpaceForNode(world, pos))
			{
				continue;
			}
			
			NavigationNode* node = new NavigationNode(Vector3((float) x, 0.0f, (float) z), pos);
			NavigationNode* leftNode = getNode(x-1, z);
			NavigationNode* topNode = getNode(x, z-1);
			
			if (leftNode != NULL && !rayTest(world, leftNode->center, node->center))
			{
				Vector3 distance = node->center - leftNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, leftNode, cost));
				leftNode->addConnection(Connection(leftNode, node, cost));
			}
			if (topNode != NULL && !rayTest(world, topNode->center, node->center))
			{
				Vector3 distance = node->center - topNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, topNode, cost));
				topNode->addConnection(Connection(topNode, node, cost));
			}

			grid[x + z * gridWidth] = node;
		}
	}

	debugDraw();
}

template<class T>
static T round(T r) 
{
	return (r > (T) 0.0) ? floor(r + (T) 0.5) : ceil(r - (T) 0.5);
}

NavigationNode* NavigationGraph::getNodeAt(const Vector3& position)
{
	int idxX = (int) round((position.x - origin.x - NavigationNode::NODE_SIZE_HALF) / NavigationNode::NODE_SIZE);
	int idxZ = (int) round((position.z - origin.z - NavigationNode::NODE_SIZE_HALF) / NavigationNode::NODE_SIZE);
	
	return getNode(idxX, idxZ);
}

NavigationNode* NavigationGraph::getNode(int idxX, int idxZ)
{
	if ((idxX >= 0) && (idxX <= gridWidth) && (idxZ >= 0) && (idxZ <= gridDepth))
	{
		return grid[idxX + idxZ * gridWidth];
	}
	else
	{
		return NULL;
	}
}

void NavigationGraph::debugDraw() const
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		if (grid[i] != NULL)
		{
			grid[i]->debugDraw(mDebugDisplay);
		}
	}

	mDebugDisplay->build();
}

std::vector<Vector3> NavigationGraph::calcPath(const Vector3& currentPosition, const Vector3& targetPosition)
{
	NavigationNode* start = getNodeAt(currentPosition);
	NavigationNode* goal = getNodeAt(targetPosition);

	std::vector<Vector3> path;

	if (start == NULL || goal == NULL)
	{
		return path;
	}

	// implement a A*
	
	return path;

}
