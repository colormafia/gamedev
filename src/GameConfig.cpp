#include "stdafx.h"
#include "GameConfig.h"
#include <OgreResourceGroupManager.h>
#include <OgreStringConverter.h>
 
template<> GameConfig* Ogre::Singleton<GameConfig>::msSingleton = 0;


GameConfig::GameConfig()
{
	m_ConfigFile.load("../../media/game.cfg", "=", true);
 
	Ogre::ConfigFile::SectionIterator seci = m_ConfigFile.getSectionIterator();
	Ogre::String sectionName;
	Ogre::String keyName;
	Ogre::String valueName;
 
	while (seci.hasMoreElements())
	{
		sectionName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			keyName = i->first;
			valueName = i->second;
			m_Configs.insert(std::pair<std::string, std::string>(sectionName + "/" + keyName, valueName));
		}
	}
 
}
 
GameConfig::~GameConfig() { }
 
 
bool GameConfig::getKeyExists(std::string key)
{
	if (m_Configs.count(key) > 0)
	{
		return true;
	}
	return false;
}
 
std::string GameConfig::getValueAsString(std::string key)
{
	if (getKeyExists(key) == true)
	{
		return m_Configs[key];
	}
	else
	{
		throw Ogre::Exception(Ogre::Exception::ERR_ITEM_NOT_FOUND,"Configuration key: " + key + " not found", "MyConfig::getValue");
	}
 
}
 
int GameConfig::getValueAsInt(std::string key)
{
	return atoi(getValueAsString(key).c_str());
}
Ogre::Real GameConfig::getValueAsReal(std::string key)
{
	return Ogre::StringConverter::parseReal(getValueAsString(key));
}
bool GameConfig::getValueAsBool(std::string key)
{
	return Ogre::StringConverter::parseBool(getValueAsString(key));
}
Ogre::Vector3 GameConfig::getValueAsVector3(std::string key)
{
	return Ogre::StringConverter::parseVector3(getValueAsString(key));
}
Ogre::ColourValue GameConfig::getValueAsColourValue(std::string key)
{
	return Ogre::StringConverter::parseColourValue(getValueAsString(key));
}