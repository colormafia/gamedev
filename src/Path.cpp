#include "stdafx.h"
#include "Path.h"
#include "MathUtil.h"
#include "DebugDisplay.h"

Path::Path():
	mType(PATH_NORMAL)
{}

Path::Path(const std::vector<Ogre::Vector3>& waypoints, PathType type):
	mType(type)
{
	assert(waypoints.size() > 1);

	mWaypoints = waypoints;

	if (mType == PATH_LOOP)
	{
		if (mWaypoints.front().distance(mWaypoints.back()) > 0.001f)
		{
			// if loop path last waypoint must be the same as the first waypoint
			mWaypoints.push_back(mWaypoints[0]);
		}
	}

	mDistances = std::vector<float>();
	mDistances.reserve(mWaypoints.size());

	mDistances.push_back(0);
	Ogre::Vector3 p = mWaypoints[0];

	for (int i = 1; i < (int) mWaypoints.size(); i++)
	{
		mDistances.push_back(p.distance(mWaypoints[i]) + mDistances[i-1]);
		p = mWaypoints[i];
	}

	mTotalLength = mDistances.back();
}


float Path::getParam(const Ogre::Vector3& position, float lastParam) const
{
	// TODO needs optimization and clean up

	lastParam = mapParam(lastParam);
	float param = 0.0f;
	float distance = 10000000.0f;

	int offset = 0;
	for (int i = 1; i < (int) mDistances.size()-1; i++)
	{
		if (mDistances[i] > lastParam)
		{
			break;
		}
		offset++;
	}

	param = mDistances[offset];
	float paramOffset = param;

	for (int i = 1; i < (int) mWaypoints.size(); i++)
	{
		int idx1 = (offset + i - 1) % mWaypoints.size();
		int idx2 = (offset + i) % mWaypoints.size();

		float t = MathUtil::getClosestPointOnLineSegment(mWaypoints[idx1], mWaypoints[idx2], position);
		Ogre::Vector3 projected = mWaypoints[idx1] * (1-t) + mWaypoints[idx2] * (t);
		float newDistance = position.squaredDistance(projected);

		float segmentLength = MathUtil::mapRange(mDistances[idx2]- mDistances[idx1], mTotalLength);

		if (newDistance < distance)
		{
			distance = newDistance;
			
			param = paramOffset + segmentLength * t;
			
		}
		else if (distance < 10.0f)
		{
			break;
		}

		paramOffset += segmentLength;
	}

	return mapParam(std::max(param, lastParam));
}

Ogre::Vector3 Path::getPosition(float param) const
{
	param = mapParam(param);

	int idx = 0;
	for (int i = 1; i < (int) mDistances.size()-1; i++)
	{
		if (mDistances[i] > param)
		{
			break;
		}
		idx++;
	}

	float sequementLength = mDistances[idx+1] - mDistances[idx];
	float t = (param - mDistances[idx]) / sequementLength;

	return mWaypoints[idx] * (1-t) + mWaypoints[idx+1] * (t);
}


bool Path::isPathEnd(float param) const
{
	if (mType == PATH_NORMAL)
	{
		return param >= mTotalLength;
	}
	return false;
}

float Path::mapParam(float param) const
{
	if (mType == PATH_NORMAL)
	{
		return std::min(param, mTotalLength);
	}
	else
	{
		return MathUtil::mapRange(param, mTotalLength);
	}
}

void Path::debugDraw() const
{
	if (!isEmpty())
	{
		DebugDisplay::getSingleton().drawCircle(mWaypoints[0], 1.0f, 8, Ogre::ColourValue(0, 0, 0, 1));

		for (int i = 1; i < (int) mWaypoints.size(); i++)
		{
			float t = (float) i / (float) mWaypoints.size();
			DebugDisplay::getSingleton().drawLine(mWaypoints[i-1], mWaypoints[i], Ogre::ColourValue(t, 0, 0, 1));
			DebugDisplay::getSingleton().drawCircle(mWaypoints[i], 1.0f, 8, Ogre::ColourValue(t, 0, 0, 1));
		}
	}
}
