#include "stdafx.h"

#include "PathfindingList.h"


void PathfindingList::remove(NavigationNode* node)
{
	std::vector<NodeRecord>::iterator it;
	for (it = nodes.begin(); it != nodes.end(); it++)
	{
		if ((*it).node == node)
		{
			nodes.erase(it);
			return;
		}
	}
}

PathfindingList::NodeRecord PathfindingList::getSmallest()
{
	if (isEmpty())
	{
		return NodeRecord();
	}
	
	NodeRecord s = nodes[0];
	
	for (int i = 1; i < (int) nodes.size(); i++)
	{
		if (nodes[i].estimatedTotalCost < s.estimatedTotalCost)
		{
			s = nodes[i];
		}
	}
	
	return s;
}

PathfindingList::NodeRecord PathfindingList::getSmallestPerCostSoFar()
{
	if (isEmpty())
	{
		return NodeRecord();
	}
	
	NodeRecord s = nodes[0];
	
	for (int i = 1; i < (int) nodes.size(); i++)
	{
		if (nodes[i].costSoFar < s.costSoFar)
		{
			s = nodes[i];
		}
	}
	
	return s;
}

PathfindingList::NodeRecord PathfindingList::find(NavigationNode* node)
{
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		if (nodes[i].node == node)
		{
			return nodes[i];
		}
	}
	
	return NodeRecord();
}

bool PathfindingList::contains(NavigationNode* node)
{
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		if (nodes[i].node == node)
		{
			return true;
		}
	}
	
	return false;
}