#include "stdafx.h"


#include "OgreBulletDynamicsRigidBody.h"				 // for OgreBullet
#include "Shapes/OgreBulletCollisionsStaticPlaneShape.h" // for static planes
#include "Shapes/OgreBulletCollisionsBoxShape.h"		 // for Boxes
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "DebugOverlay.h"
#include "DebugDisplay.h"
#include "GameApplication.h"
#include "GameConfig.h"

#include "OgreConsole.h"
#include "Spacecraft.h"
#include "SpacecraftController.h"
#include "HumanController.h"
#include "AIController.h"
#include "Rocket.h"
#include "Wall.h"
#include "CommandCenter.h"
#include "ICollider.h"
#include "WorldUtil.h"

#include "LuaScriptManager.h"
#include "NavigationGraph.h"
#include "FileWatcher/FileWatcher.h"

/// Processes a file action
class TextureReloader : public FW::FileWatchListener
{
public:
	TextureReloader() {}
	void handleFileAction(FW::WatchID watchid, const FW::String& dir, const FW::String& filename, FW::Action action)
	{
		// Get the texture with this name (if it exists)
		Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName(filename);

		// this is actually a bug. Sometimes the event is received before the system is
		// finished writing the change and you will get file contention errors. So, wait
		// for the write to finish.
		Sleep(100);

		// if the texture exists, reload it
		if (!tex.isNull())
			tex->reload();
	}
};

// where UpdateListener is defined as such
class UpdateListener : public FW::FileWatchListener
{
public:
	UpdateListener() {}
	void handleFileAction(FW::WatchID watchid, const FW::String& dir, const FW::String& filename,
		FW::Action action)
	{
		switch (action)
		{
		case FW::Actions::Add:
			std::cout << "File (" << dir + "\\" + filename << ") Added! " << std::endl;
			break;
		case FW::Actions::Delete:
			std::cout << "File (" << dir + "\\" + filename << ") Deleted! " << std::endl;
			break;
		case FW::Actions::Modified:
			std::cout << "File (" << dir + "\\" + filename << ") Modified! " << std::endl;
			break;
		default:
			std::cout << "Should never happen!" << std::endl;
		}
	}
};

/// Processes a file action


template<> GameApplication* Ogre::Singleton<GameApplication>::msSingleton = 0;
GameApplication* GameApplication::getSingletonPtr(void)
{
    return msSingleton;
}
GameApplication& GameApplication::getSingleton(void)
{  
   assert(msSingleton);  
   return *msSingleton;  
}

GameApplication::GameApplication(Mode mode, String address):
	mHumanController(NULL),
	mRocketCounter(0),
	mWorld(NULL),
	mDebugDrawer(NULL),
	mDebugOverlay(NULL),
	mScriptingManager(NULL),
	mShowDebugDraw(false),
	mShowNavigationGraph(false),
	mFollowPlayerCam(false),
	mSynchTimer(0.0f),
	mMode(mode),
	mAddress(address)
{
	mScriptingManager = new scripting::Manager();
	// Create the object
	fileWatcher = new FW::FileWatcher();

	// add a directory watch
	watchid = fileWatcher->addWatch("../../media", new TextureReloader());
	watchid = fileWatcher->addWatch("../../media", new UpdateListener());
}

GameApplication::~GameApplication()
{
	for (size_t i = 0; i < mControllers.size(); i++)
	{
		delete mControllers[i];
	}

	for (size_t i = 0; i < mSpacecrafts.size(); i++)
	{
		delete mSpacecrafts[i];
	}

	std::list<Rocket*>::iterator it;

	for (it = mRockets.begin(); it != mRockets.end(); ++it)
	{
		delete *it;
	}

	mReleasedRockets.unique();
	for (it = mReleasedRockets.begin(); it != mReleasedRockets.end(); ++it)
	{
		mRockets.remove(*it);
		delete *it;
	}

	mControllers.clear();
	mSpacecrafts.clear();
	mRockets.clear();
	mReleasedRockets.clear();

	SAFE_DELETE(mDebugDrawer);

	SAFE_DELETE(mWorld);

}

void GameApplication::createScene(void)
{
	// Set ambient light
 	mSceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
 
 	// Create a light
 	Light* l = mSceneMgr->createLight("MainLight");
 	l->setPosition(20,80,50);

	new DebugDisplay(mSceneMgr, 0.5f);
	DebugDisplay::getSingleton().setEnabled(false);

	mDebugOverlay = new DebugOverlay();

	new OgreConsole;
	OgreConsole::getSingleton().init(mRoot);
	OgreConsole::getSingleton().setVisible(false);

	createDynamicWorld(Vector3(0.0f, -9.81f, 0.0f), AxisAlignedBox (Ogre::Vector3 (-10000, -10000, -10000), Ogre::Vector3 (10000,  10000,  10000)));
	createWalls();

	// now create navigation graph
	new NavigationGraph(mSceneMgr, -400, -600, 800, 800);
	NavigationGraph::getSingleton().calcGraph(mWorld);
	NavigationGraph::getSingleton().setDebugDisplayEnabled(mShowNavigationGraph);

	createSpacecrafts();

	createExternMesh();

	InitializeLuaScripts();

	Ogre::CompositorManager::getSingleton().addCompositor(vpTopView, "Invert");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(vpTopView, "Invert", true);
}

void GameApplication::InitializeLuaScripts()
{
	mScriptingManager->runScriptFile("../../Media/skipper.lua");
}

void GameApplication::createExternMesh() {
	Ogre::SceneNode* pNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	Ogre::Light* pDirLight = mSceneMgr->createLight();
	pDirLight->setType(Ogre::Light::LT_DIRECTIONAL);
	pDirLight->setDirection(Ogre::Vector3(0, 0, -200));
	pNode->setPosition(Ogre::Vector3(0, 18, -202));
	pNode->attachObject(pDirLight);

	Ogre::Entity* entity = mSceneMgr->createEntity("skipper", "penguin.mesh");
	pNode->attachObject(entity);	
}

void GameApplication::createDynamicWorld(Vector3 &gravityVector,AxisAlignedBox &bounds)
{
 	// Start Bullet
 	mWorld = new OgreBulletDynamics::DynamicsWorld(mSceneMgr, bounds, gravityVector);
 
 	 // add Debug info display tool
 	mDebugDrawer = new OgreBulletCollisions::DebugDrawer();
 	mDebugDrawer->setDrawWireframe(true);	// we want to see the Bullet containers
 
 	mWorld->setDebugDrawer(mDebugDrawer);
 	mWorld->setShowDebugShapes(false);		// enable it if you want to see the Bullet containers
 	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("debugDrawer", Ogre::Vector3::ZERO);
 	node->attachObject(static_cast <SimpleRenderable*> (mDebugDrawer));
 
    // Define a floor plane mesh
 	Entity *ent;
    Plane p;
    p.normal = Vector3(0,1,0); p.d = 0;
    MeshManager::getSingleton().createPlane("FloorPlane", 
                                            ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
                                            p, 200000, 200000, 20, 20, true, 1, 9000, 9000, 
                                            Vector3::UNIT_Z);
    // Create an entity (the floor)
    ent = mSceneMgr->createEntity("floor", "FloorPlane");
 	ent->setMaterialName("ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
 
 	// add collision detection to it
 	OgreBulletCollisions::CollisionShape *Shape;
 	Shape = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0,1,0), 0);
 	// a body is needed for the shape
	OgreBulletDynamics::RigidBody *defaultPlaneBody = new OgreBulletDynamics::RigidBody("BasePlane", mWorld, WorldUtil::FILTER_STATIC_GROUND, WorldUtil::FILTER_ALL);

 	defaultPlaneBody->setStaticShape(Shape, 0.0f, 0.8f); // (shape, restitution, friction)
}

void GameApplication::createWalls()
{
	CommandCenter* commandcenter = new CommandCenter(mSceneMgr, mWorld, Vector3(0.0f, 0.0f, -200.0f));
	Wall* wall;

	wall = new Wall(mSceneMgr, mWorld, Vector3(100.0f, 5.0f, -100.0f), Vector3(100.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(100.0f, 5.0f, -220.0f), Vector3(100.0f, 5.0f, -300.0f));

	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -100.0f), Vector3(-100.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -220.0f), Vector3(-100.0f, 5.0f, -300.0f));

	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -100.0f), Vector3(-20.0f, 5.0f, -100.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(20.0f, 5.0f, -100.0f), Vector3(100.0f, 5.0f, -100.0f));
		
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -300.0f), Vector3(-20.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(20.0f, 5.0f, -300.0f), Vector3(100.0f, 5.0f, -300.0f));
}


void GameApplication::createSpacecrafts()
{
	Spacecraft* craft1 = new Spacecraft(0, "craft1", mSceneMgr, mWorld, Vector3(0.0f, 10.0f, 0.0f), "spacecraft/red");
	craft1->setOrientation(Quaternion(Radian(Math::PI), Vector3(0.0f, 1.0f, 0.0f)));

	Spacecraft* craft2 = new Spacecraft(1, "craft2", mSceneMgr, mWorld, Vector3(-50.0f, 10.0f, -150.0f), "spacecraft/blue");
	Spacecraft* craft3 = new Spacecraft(2, "craft3", mSceneMgr, mWorld, Vector3(50.0f, 10.0f, -250.0f), "spacecraft/blue");

	mSpacecrafts.push_back(craft1);
	mSpacecrafts.push_back(craft2);
	mSpacecrafts.push_back(craft3);
	//mSpacecrafts.push_back(craft2);
	// create controllers
	mHumanController = new HumanController(craft1);
	mControllers.push_back(mHumanController);


	static const Ogre::Vector3 TARGETS[] = {
		Ogre::Vector3(50.0f, 5.0f, -150.0f),
		Ogre::Vector3(50.0f, 5.0f, -250.0f),
		Ogre::Vector3(-50.0f, 5.0f, -250.0f),
		Ogre::Vector3(-50.0f, 5.0f, -150.0f)
	};

	std::vector<Ogre::Vector3> waypoints1;
	waypoints1.push_back(TARGETS[0]);
	waypoints1.push_back(TARGETS[1]);
	waypoints1.push_back(TARGETS[2]);
	waypoints1.push_back(TARGETS[3]);
	
	std::vector<Ogre::Vector3> waypoints2;
	waypoints2.push_back(TARGETS[3]);
	waypoints2.push_back(TARGETS[2]);
	waypoints2.push_back(TARGETS[1]);
	waypoints2.push_back(TARGETS[0]);


	AIController* aiController1 = new AIController(craft2, craft1, Path(waypoints1, Path::PATH_LOOP));
	mControllers.push_back(aiController1);
	AIController* aiController2 = new AIController(craft3, craft1, Path(waypoints2, Path::PATH_LOOP));
	mControllers.push_back(aiController2);
}


void GameApplication::createCamera(void)
{
    BaseApplication::createCamera();
	mCamera->setPosition(Ogre::Vector3(0, 50, 100));

	mMapCam = mSceneMgr->createCamera("MapCam");
	mMapCam->setPosition(0, 250, -200);
	mMapCam->lookAt(0, 0, -201);

	vpTopView = mWindow->addViewport(mMapCam, 1, 0.75, 0, 0.25, 0.25);
	vpTopView->setBackgroundColour(Ogre::ColourValue(0, 0, 0));
	vpTopView->setOverlaysEnabled(false);
	setDragLook(mGameConfig->getValueAsBool("Game/DragLook"));
}


bool GameApplication::frameStarted(const Ogre::FrameEvent& evt)
{
   bool ret = BaseApplication::frameStarted(evt);

   float delta = evt.timeSinceLastFrame;
   update(delta);

   // Right before the frame is rendered, call DebugDrawer::build().
   DebugDisplay::getSingleton().build();
   return ret;
}
 

void GameApplication::update(float delta)
{

	//File watcher update
	fileWatcher->update();

	//run scripts
	if (mScriptingManager->hasGlobalFunction("update"))
	{
		mScriptingManager->GetGlobalTable()["update"](delta, mSceneMgr->getEntity("skipper"));		
	}


	// update controllers
	for (size_t i = 0; i < mControllers.size(); i++)
	{
		mControllers[i]->update(delta);
	}

	// update spacecrafts
	for (size_t i = 0; i < mSpacecrafts.size(); i++)
	{
		mSpacecrafts[i]->update(delta);
	}

	// update rockets
	for (std::list<Rocket*>::iterator it = mRockets.begin(); it != mRockets.end(); it++)
	{
		(*it)->update(delta);
	}


	// update Bullet Physics animation
	mWorld->stepSimulation(delta, 10);	

	// call onCollision of colliders
	btDynamicsWorld* dynamicWorld = mWorld->getBulletDynamicsWorld();

	int numManifolds = dynamicWorld->getDispatcher()->getNumManifolds();
	for (int i=0;i<numManifolds;i++)
	{
		btPersistentManifold* contactManifold =  dynamicWorld->getDispatcher()->getManifoldByIndexInternal(i);
		const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
		const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());
	
		int numContacts = contactManifold->getNumContacts();
		for (int j=0;j<numContacts;j++)
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance()<0.f)
			{
				const btVector3& ptA = pt.getPositionWorldOnA();
				const btVector3& ptB = pt.getPositionWorldOnB();
				const btVector3& normalOnB = pt.m_normalWorldOnB;

				void* pointer0 = contactManifold->getBody0()->getUserPointer();
				void* pointer1 = contactManifold->getBody1()->getUserPointer();
				ICollider* collider0 = (pointer0 != NULL) ? static_cast<ICollider*>(pointer0) : NULL;
				ICollider* collider1 = (pointer1 != NULL) ? static_cast<ICollider*>(pointer1) : NULL;

				if (collider0 != NULL)
				{
					collider0->onCollision(collider1);
				}

				if (collider1 != NULL)
				{
					collider1->onCollision(collider0);
				}
			}
		}
	}

	// delete rockets if neccessary
	if (mReleasedRockets.size() > 0)
	{
		mReleasedRockets.unique();
		std::list<Rocket*>::iterator it;

		for (it = mReleasedRockets.begin(); it != mReleasedRockets.end(); ++it)
		{
			mRockets.remove(*it);
			delete *it;
		}

		mReleasedRockets.clear();
	}

	if (mHumanController != NULL)
	{
		Spacecraft* craft = mHumanController->getSpacecraft();

		if (mFollowPlayerCam)
		{
			// look at human craft.
			mCamera->setPosition(craft->getPosition() - craft->getDirection() * Vector3(50.0f, 0.0f, 50.0f) + Vector3(0.0f, 20.0f, 0.0f));
			mCamera->lookAt(craft->getPosition());
		}
	}
}


bool GameApplication::frameRenderingQueued(const FrameEvent& evt)
{
 	bool ret = BaseApplication::frameRenderingQueued(evt);
 	return ret;
}

bool GameApplication::frameEnded(const Ogre::FrameEvent& evt)
{
	bool ret = BaseApplication::frameEnded(evt);
   // After the frame is rendered, call DebugDrawer::clear()
   DebugDisplay::getSingleton().clear();

   return ret;
}

bool GameApplication::keyPressed(const OIS::KeyEvent &arg)
{
	if ((mHumanController != NULL) && mHumanController->keyPressed(arg))
	{
		return true;
	}

	return BaseApplication::keyPressed(arg);
}

bool GameApplication::keyReleased(const OIS::KeyEvent &arg)
{
	if ((mHumanController != NULL) && mHumanController->keyReleased(arg))
	{
		return true;
	}

	if (arg.key == OIS::KC_1)
	{
		mShowDebugDraw = !mShowDebugDraw;
		mWorld->setShowDebugShapes(mShowDebugDraw);
		DebugDisplay::getSingleton().setEnabled(mShowDebugDraw);
	}

	if (arg.key == OIS::KC_2)
	{
		mShowNavigationGraph = !mShowNavigationGraph;
		NavigationGraph::getSingleton().setDebugDisplayEnabled(mShowNavigationGraph);
	}

	if (arg.key == OIS::KC_3)
	{
		OgreConsole& console = OgreConsole::getSingleton();
		console.setVisible(!console.isVisible());
	}

	if (arg.key == OIS::KC_4)
	{
		mScriptingManager->runScriptFile("../../media/controller.lua");
	}

	if (arg.key == OIS::KC_TAB)
	{
		mFollowPlayerCam = !mFollowPlayerCam;
	}

	return BaseApplication::keyReleased(arg);
}

void GameApplication::createRocket(const Vector3& position, const Vector3& direction)
{
	if ((int) mRockets.size() >= mGameConfig->getValueAsInt("Rocket/MaxRockets"))
	{
		Rocket* oldestRocket = *mRockets.begin();
		releaseRocket(oldestRocket);
	}

	Ogre::String name = "Rocket_";
	name += (mRocketCounter++);
	Rocket* rocket = new Rocket(name, mSceneMgr, mWorld, position, direction);
	mRockets.push_back(rocket);
}

void GameApplication::releaseRocket(Rocket* rocket)
{
	mReleasedRockets.push_back(rocket);
}
 
bool GameApplication::configure(void)
{
	mGameConfig = new GameConfig();

	bool hasConfig = false;

	if (!mGameConfig->getValueAsBool("Game/ShowConfigDialog"))
	{
		hasConfig = mRoot->restoreConfig();
	}

	if (!hasConfig)
	{
		hasConfig = mRoot->showConfigDialog();
	}

	if(hasConfig)
    {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'

		const String titles[] = { "Spacecrafts", "SC-Server", "SC-Client" };
        mWindow = mRoot->initialise(true, titles[mMode]);


		mWindow->setDeactivateOnFocusChange(false);
 		// Let's add a nice window icon
        return true;
    }
    else
    {
        return false;
    }
}
 
 
 #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
 #define WIN32_LEAN_AND_MEAN
 #include "windows.h"
 #include "shellapi.h"
 #endif
 
 #ifdef __cplusplus
 	extern "C" {
 #endif
 
 #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
 		INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
		{
			LPWSTR* argvw;
			int argc;
			argvw = CommandLineToArgvW(GetCommandLineW(), &argc);

			GameApplication::Mode mode = GameApplication::MODE_STANDALONE;
			std::string address = "127.0.0.1";

			if (argc > 1)
			{
				if (wcscmp(argvw[1], L"server") == 0)
				{
					mode = GameApplication::MODE_SERVER;
				}
				else if (wcscmp(argvw[1], L"client") == 0)
				{
					mode = GameApplication::MODE_CLIENT;

					if (argc > 2)
					{
						std::wstring ws = std::wstring(argvw[2]);
						address = std::string(ws.begin(), ws.end());
					}
				}
			}

 #else
 		int main(int argc, char *argv[])
		{
 #endif
 			// Create application object
 			GameApplication app(mode, address);
 
 			try {
 				app.go();
 			} catch( Ogre::Exception& e ) {
 #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
 				MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
 #else
 				std::cerr << "An exception has occured: " <<
 					e.getFullDescription().c_str() << std::endl;
 #endif
 			}
 
 			return 0;
 		}
 
 #ifdef __cplusplus
 	}
 #endif