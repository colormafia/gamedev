#include "StdAfx.h"

#include "CommandCenter.h"

using namespace Ogre;

int CommandCenter::msInstanceCounter = 0;

CommandCenter::CommandCenter(Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const Ogre::Vector3& position)
{
	String name = "commandcenter_" + StringConverter::toString(msInstanceCounter++);

	Entity *entity = sceneMgr->createEntity(name, "command_center.mesh");			    
 	entity->setCastShadows(false);
	entity->setMaterialName("command_center");

	mNode = sceneMgr->getRootSceneNode()->createChildSceneNode();
 	mNode->attachObject(entity);
	//mNode->scale(scale);

	Ogre::Vector3 offset(0.0f, entity->getBoundingBox().getHalfSize().y, 0.0f);
	mNode->setPosition(position + offset);

	// after that create the Bullet shape with the calculated size
	mShape = new OgreBulletCollisions::BoxCollisionShape(entity->getBoundingBox().getHalfSize());
    // and the Bullet rigid body
    mBody = new OgreBulletDynamics::RigidBody(name, world);
    mBody->setStaticShape(mNode, mShape, 0.1f, 0.0f, position + offset);
	btRigidBody* rigidBody = mBody->getBulletRigidBody();
	rigidBody->setUserPointer(this);
}

