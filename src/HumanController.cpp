#include "StdAfx.h"

#include "HumanController.h"

#include "DebugDisplay.h"
#include "GameApplication.h"
#include "WorldUtil.h"
#include "MathUtil.h"
#include "GameConfig.h"

using namespace Ogre;

HumanController::HumanController(Spacecraft* spacecraft):
	SpacecraftController(spacecraft),
	mSteer(0.0f),
	mMove(0.0f)
{}


void HumanController::update(float delta)
{
	if (GameConfig::getSingleton().getValueAsBool("HumanController/DirectControl"))
	{
		Vector3 direction = mSpacecraft->getDirection();
		Vector3 desiredVelocity = direction * mMove * Spacecraft::MAX_SPEED;
		Vector3 currentVelocity = mSpacecraft->getLinearVelocity();
		float desiredRotation = mSteer * Spacecraft::MAX_ANGULAR_SPEED;
		float currentRotation = mSpacecraft->getAngularVelocity();

		mSpacecraft->setSteeringCommand(desiredVelocity - currentVelocity, MathUtil::mapAngle(desiredRotation - currentRotation));
		DebugDisplay::getSingleton().drawLine(mSpacecraft->getPosition(), mSpacecraft->getPosition() + desiredVelocity * 1.0f, ColourValue(0.0f, 1.0f, 0.0f));
	}
	else
	{
		Vector3 direction = mSpacecraft->getDirection();
		Vector3 desiredVelocity = direction * std::max(0.001f, mMove) * Spacecraft::MAX_SPEED;
		Vector3 currentVelocity = mSpacecraft->getLinearVelocity();
		DebugDisplay::getSingleton().drawLine(mSpacecraft->getPosition(), mSpacecraft->getPosition() + desiredVelocity * 10.0f, ColourValue(0.0f, 1.0f, 0.0f));

		float speed = mSpacecraft->getLinearVelocity().length();
		float strength = Math::Clamp(1.0f - speed / Spacecraft::MAX_SPEED, 0.0f, 1.0f) / 0.75f + 0.25f;
		float steer = Math::Clamp((float)mSteer, -strength, strength);
		desiredVelocity = Quaternion(Ogre::Radian(Math::PI * steer * 0.25f), Ogre::Vector3(0.0f, 1.0f, 0.0f)) * desiredVelocity;

		mSpacecraft->setSteeringCommand((desiredVelocity - currentVelocity) + obstacleAvoidance() + wallAvoidance());
	}
}

bool HumanController::keyPressed(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_A)
	{
		mSteer = 1.0f;
		return true;
	}

	if (arg.key == OIS::KC_D)
	{
		mSteer = -1.0f;
		return true;
	}

	if (arg.key == OIS::KC_W)
	{
		mMove = 1.0f;
		return true;
	}

	if (arg.key == OIS::KC_S)
	{
		mMove = -1.0f;
		return true;
	}

	return false;
}

bool HumanController::keyReleased(const OIS::KeyEvent &arg)
{
	if ((arg.key == OIS::KC_A) && (mSteer > 0.0f))
	{
		mSteer = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_D) && (mSteer < 0.0f))
	{
		mSteer = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_W) && (mMove > 0.0f))
	{
		mMove = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_S) && (mMove < 0.0f))
	{
		mMove = 0.0f;
		return true;
	}

	if (arg.key == OIS::KC_SPACE)
	{
		mSpacecraft->shoot();
		return true;
	}

	return false;
}