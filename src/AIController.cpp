#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"

using namespace Ogre;

AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mCurrentParam(0.0f),
	mPath(patrol)
{}

void AIController::update(float delta)
{
	Vector3 linearSteering = followPath(mPath, mCurrentParam);
	//Vector3 seek = 
	getSpacecraft()->setSteeringCommand(linearSteering);
}


 
